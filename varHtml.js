const variablesEnCodigoHTML = {
    titleHTMLIndex: 'Utilizando Handlebars con Roadtrip Template',
    titleHTMLGeneric: 'Generic - Ejemplo texto Genérico en Template',
    titleHTMLElements: 'Elements - Ejemplos de diseño CSS en Template',

    firstTitleIndex: 'Utilizando Handlebars con Roadtrip Template',
    firstTitleGeneric: 'Volver HOME',
    firstTitleElements: 'Volver HOME'
}

module.exports = {
    variablesEnCodigoHTML
}