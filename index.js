// Requires
require('colors');
require('dotenv').config();
const express = require('express');
const hbs = require('hbs');
const { variablesEnCodigoHTML } = require('./varHtml');

//Variables
const app = express();
const puerto = process.env.PORT;
const publicPath = __dirname + '/public';
const viewsPath = __dirname + '/views';

//MAIN
const main = async() => {
    // Handlebars
    app.set('view engine', 'hbs');

    // Middlewares
    //app.use('/', express.static(publicPath)); // Utilizar esta línea con el código para PUBLIC
    app.use(express.static(viewsPath)); // Utilizar esta línea con el código para VIEWS (hbs)
    hbs.registerPartials(viewsPath + '/parciales'); // Utilizar esta línea con el código para VIEWS (hbs)

    // Rutas
    // Código para VIEWS (hbs)
    app.get('/', (req, res) => {
        res.render('index', {
            titleHTML: variablesEnCodigoHTML.titleHTML,
            firstTitle: variablesEnCodigoHTML.firstTitleIndex
        });
    });

    app.get('/generic', (req, res) => {
        res.render('generic', {
            titleHTML: variablesEnCodigoHTML.titleHTMLGeneric,
            firstTitle: variablesEnCodigoHTML.firstTitleGeneric
        });
    });

    app.get('/elements', (req, res) => {
        res.render('elements', {
            titleHTML: variablesEnCodigoHTML.titleHTMLElements,
            firstTitle: variablesEnCodigoHTML.firstTitleElements
        });
    });

    app.get('*', (req, res) => {
        res.render('404');
    });

    // Código para PUBLIC
    /*app.get('/generic', (req, res) => {
        res.sendFile(publicPath + '/generic.html');
    })

    app.get('/elements', (req, res) => {
        res.sendFile(publicPath + '/elements.html');
    })

    app.get('*', (req, res) => {
        res.sendFile(publicPath + '/404.html');
    })*/

    app.listen(puerto, () => {
        console.log(`\nServidor En Linea.\nEscuchando el puerto ${(puerto + '').yellow} ...`);
    });
}

main();